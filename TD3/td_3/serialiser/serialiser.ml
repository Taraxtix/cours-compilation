(** Use this function to translate a list of integer of size 8 representing the integer as a list of bytes (between 0 and 255) that can be converted to char.*)
let int_list_of_int i =
  let rec normalise_list list =
    if List.length list >= 8 then list else normalise_list (0 :: list)
  in
  let rec aux rest acc =
    if rest = 0 then acc else aux (rest / 256) ((rest mod 256) :: acc)
  in
  if i >= 0 then normalise_list (aux i [])
  else
    let l = normalise_list (aux (i + max_int + 1) []) in
    (List.hd l + 64) :: List.tl l

(** Decodes an list of integers as an int. Use it to control your results.*)
let int_of_int_list l =
  let rec aux l acc =
    if l = [] then acc else aux (List.tl l) ((256 * acc) + List.hd l)
  in
  aux l 0

(** Converts a float in an list of integer of size 8 that can be converted to char (the content are between 0 and 255)*)
let int_list_of_float f = int_list_of_int Int64.(to_int (bits_of_float f))

(** Decodes the previous encoding.*)
let float_of_int_list l = Int64.(float_of_bits (of_int (int_of_int_list l)))

(** Encodes a string as a list of ints between 1 and 255. The list is terminated by a 0.*)
let int_list_of_string s =
  List.map int_of_char (List.of_seq (String.to_seq s)) @ [ 0 ]

(** Converts the content of file [source] to a int list, as described in the subject.
      You have first to open the source with open_in, and get a lexbuf with the module Lexing (see other files of this exercice session). Then you have to call the lexer and then converts each token to an int. Use the functions above to converts integers, floats and strings that are arguments of print_string.
      The only difficult point is for identifiers: you have to keep track of each identifier and attribute to each a unique integer (for exemple its position in the list of identifiers when ordering them by order of first appearance). You should support more than 255 identifiers, so you should have an encoding terminated by a particular number - e.g 0 or 255. Thus you cannot use the functions above for that.*)
let serialise_to_int_list source =
  let _, lexbuf = MenhirLib.LexerUtil.read source in
  let id_map = Hashtbl.create 256 in
  let rec aux list =
    match Course_language.Lexer.token lexbuf with
    | Course_language.Parser.IF -> aux (list @ [ 0 ])
    | Course_language.Parser.THEN -> aux (list @ [ 1 ])
    | Course_language.Parser.ELSE -> aux (list @ [ 2 ])
    | Course_language.Parser.WHILE -> aux (list @ [ 3 ])
    | Course_language.Parser.L_PAR -> aux (list @ [ 4 ])
    | Course_language.Parser.R_PAR -> aux (list @ [ 5 ])
    | Course_language.Parser.L_CUR_BRK -> aux (list @ [ 6 ])
    | Course_language.Parser.R_CUR_BRK -> aux (list @ [ 7 ])
    | Course_language.Parser.L_SQ_BRK -> aux (list @ [ 8 ])
    | Course_language.Parser.R_SQ_BRK -> aux (list @ [ 9 ])
    | Course_language.Parser.ADD -> aux (list @ [ 10 ])
    | Course_language.Parser.SUB -> aux (list @ [ 11 ])
    | Course_language.Parser.MUL -> aux (list @ [ 12 ])
    | Course_language.Parser.DIV -> aux (list @ [ 13 ])
    | Course_language.Parser.MOD -> aux (list @ [ 14 ])
    | Course_language.Parser.AND -> aux (list @ [ 15 ])
    | Course_language.Parser.OR -> aux (list @ [ 16 ])
    | Course_language.Parser.NOT -> aux (list @ [ 17 ])
    | Course_language.Parser.EQ -> aux (list @ [ 18 ])
    | Course_language.Parser.NEQ -> aux (list @ [ 19 ])
    | Course_language.Parser.LT -> aux (list @ [ 20 ])
    | Course_language.Parser.GT -> aux (list @ [ 21 ])
    | Course_language.Parser.LEQ -> aux (list @ [ 22 ])
    | Course_language.Parser.GEQ -> aux (list @ [ 23 ])
    | Course_language.Parser.COMMA -> aux (list @ [ 24 ])
    | Course_language.Parser.SEMICOLON -> aux (list @ [ 25 ])
    | Course_language.Parser.ASSIGN -> aux (list @ [ 26 ])
    | Course_language.Parser.DEF -> aux (list @ [ 27 ])
    | Course_language.Parser.DOT -> aux (list @ [ 28 ])
    | Course_language.Parser.PRINT -> aux (list @ [ 29 ])
    | Course_language.Parser.SIZE -> aux (list @ [ 30 ])
    | Course_language.Parser.RETURN -> aux (list @ [ 31 ])
    | Course_language.Parser.INT_TYP -> aux (list @ [ 32 ])
    | Course_language.Parser.FLOAT_TYP -> aux (list @ [ 33 ])
    | Course_language.Parser.NULL_TYP -> aux (list @ [ 34 ])
    | Course_language.Parser.BOOL_TYP -> aux (list @ [ 35 ])
    | Course_language.Parser.VAR -> aux (list @ [ 36 ])
    | Course_language.Parser.BOOL true -> aux (list @ [ 37 ])
    | Course_language.Parser.BOOL false -> aux (list @ [ 38 ])
    | Course_language.Parser.INT i -> aux (list @ [ 39 ] @ int_list_of_int i)
    | Course_language.Parser.FLOAT f -> aux (list @ [ 40 ] @ int_list_of_float f)
    | Course_language.Parser.STRING str ->
        aux (list @ [ 41 ] @ int_list_of_string str)
    | Course_language.Parser.ID id ->
        if Hashtbl.mem id_map id then
          aux (list @ [ 42 ] @ [ Hashtbl.find id_map id ])
        else
          let new_id = Hashtbl.length id_map in
          if new_id > 255 then failwith "Too many identifiers were created";
          Hashtbl.add id_map id new_id;
          aux (list @ [ 43 ] @ [ new_id ])
    | Course_language.Parser.EOF -> list
  in
  aux []

(** Detects if the programs of files [source1] and [source2] are identical up to renaming of identifiers. You should use the previous function for that.*)
let detect_plagiarism source1 source2 =
  let list1 = serialise_to_int_list source1
  and list2 = serialise_to_int_list source2 in
  List.equal Int.equal list1 list2

(** Serialises the content of file [source] and writes it in the formatter, with Format.printf. You should use [serialise_to_int_list] and converts the result to chars.*)
let serialise_to_channel formatter source =
  let int_list = serialise_to_int_list source in
  let char_list = List.map char_of_int int_list in
  let str = String.of_seq (List.to_seq char_list) in
  Format.fprintf formatter "%s" str
