

{
    exception Error of string
}

let digit = ['0' - '9']
let spaces = [' ' '\t' '\r']

rule token = parse
    | spaces            { token lexbuf }
    | digit+ as s       { int_of_string s }
    | '+'               { (token lexbuf) + (token lexbuf) }
    | '-'               { (token lexbuf) - (token lexbuf) }
    | '*'               { (token lexbuf) * (token lexbuf) }
    | '/'               { (token lexbuf) / (token lexbuf) }
    | '%'               { (token lexbuf) mod (token lexbuf) }
    | '<'               { numbers "" lexbuf }
    | _ as s            { let pos = Lexing.lexeme_start_p lexbuf in raise (Error(Format.sprintf "Line %d, char %d ,Read: '%c'. It is not an acceptable character" pos.pos_lnum (pos.pos_cnum - pos.pos_bol +1) s)) }
and numbers sofar = parse
    | spaces            { numbers sofar lexbuf }
    | "zero"            { numbers (sofar^"0") lexbuf }
    | "un"              { numbers (sofar^"1") lexbuf }
    | "deux"            { numbers (sofar^"2") lexbuf }
    | "trois"           { numbers (sofar^"3") lexbuf }
    | "quatre"          { numbers (sofar^"4") lexbuf }
    | "cinq"            { numbers (sofar^"5") lexbuf }
    | "six"             { numbers (sofar^"6") lexbuf }
    | "sept"            { numbers (sofar^"7") lexbuf }
    | "huit"            { numbers (sofar^"8") lexbuf }
    | "neuf"            { numbers (sofar^"9") lexbuf }
    | '>'               { int_of_string sofar }