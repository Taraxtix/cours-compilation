

{
    open Parser
    exception Error of string
}

(* Si vous utilisez plusieurs fois des expressions régulières, vous pouvez les nommer ici.*)
let digit = ['0'-'9']
let alpha = ['a'-'z' 'A'-'Z']
let alphanum = (alpha | digit)
let id_chars = (alphanum  | '_' | '\\')

(* Ici vous avez la règle principale. Il est possible d’en définir d’autres. La règle token s’appelle par [token lexbuf], lexbuf étant l’argument caché du lexeur (c’est le buffer de lecture). Cela appellera la règle APRÈS avoir reconnu le motif courant. C’est en particulier comme ça qu’on ignore des motifs (on reconnaît un motif, puis on se rappelle récursivement sur le suivant). *)
rule token = parse
    | [' ' '\t' '\r']               {token lexbuf}
    | '\n'                          { Lexing.new_line lexbuf ; token lexbuf }
    | "/*"                          { comment lexbuf }
    | "//" [^'\n']*                 { token lexbuf }
    | "if"                          { IF }
    | "then"                        { THEN }
    | "else"                        { ELSE }
    | "while"                       { WHILE }
    | "print"                       { PRINT }
    | "return"                      { RETURN }
    | "int"                         { INT_TYP }
    | "float"                       { FLOAT_TYP }
    | "bool"                        { BOOL_TYP }
    | "null"                        { NULL_TYP }
    | "var"                         { VAR }
    | "size"                        { SIZE }
    | '('                           { L_PAR }
    | ')'                           { R_PAR }
    | '{'                           { L_CUR_BRK }
    | '}'                           { R_CUR_BRK }
    | '['                           { L_SQ_BRK }
    | ']'                           { R_SQ_BRK }
    | '+'                           { ADD }
    | '-'                           { SUB }
    | '*'                           { MUL }
    | '/'                           { DIV }
    | '%'                           { MOD }
    | "&&"                          { AND }
    | "||"                          { OR }
    | '!'                           { NOT }
    | '='                           { EQ }
    | "<>"                          { NEQ }
    | "<="                          { LEQ }
    | ">="                          { GEQ }
    | '<'                           { LT }
    | '>'                           { GT }
    | ','                           { COMMA }
    | ';'                           { SEMICOLON }
    | ":="                          { ASSIGN }
    | "::="                         { DEF }
    | '.' ((digit+) as i)           { FLOAT (float_of_string ("0."^i)) }
    | ((digit+) '.' (digit*)) as i  { FLOAT (float_of_string i) }
    | (digit+) as i                 { INT (int_of_string i) }
    | ("true" | "false") as b       { BOOL (bool_of_string b) }
    | '"' ([^'"']* as s) '"'        { STRING s }
    | '.'                           { DOT }
    | (alpha (id_chars*)) as s      { ID s }
    | eof                           { EOF }
    | _ as s                        { raise (Error(String.make 1 s)) }

and comment = parse
    | '\n'           { Lexing.new_line lexbuf ; comment lexbuf }
    | "*/"           { token lexbuf }
    | eof            { EOF }
    (*Same as _ but takes more characters at ones*)
    | [^ '*' '\n']+  { comment lexbuf }
    | _              { comment lexbuf }