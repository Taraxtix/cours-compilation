%{
    open Ast
%}

%token IF
%token THEN
%token ELSE
%token WHILE
%token L_PAR
%token R_PAR
%token L_CUR_BRK
%token R_CUR_BRK
%token L_SQ_BRK
%token R_SQ_BRK
%token ADD
%token SUB
%token MUL
%token DIV
%token MOD
%token AND
%token OR
%token NOT
%token EQ
%token NEQ
%token LT
%token GT
%token LEQ
%token GEQ
%token COMMA
%token SEMICOLON
%token ASSIGN
%token DEF
%token DOT
%token PRINT
%token SIZE
%token RETURN
%token INT_TYP
%token FLOAT_TYP
%token BOOL_TYP
%token NULL_TYP
%token VAR
%token <string> ID
%token <string> STRING
%token <int> INT
%token <float> FLOAT
%token <bool> BOOL
%token EOF

%left AND OR
%left EQ NEQ LT GT LEQ GEQ
%left ADD SUB
%left MUL DIV MOD
%nonassoc NOT
%nonassoc UMIN

%start <Ast.t> main

%%

main:
| l = function_list EOF     { Program l (*Le List.rev est là si vous construisez la liste à l’envers (ce qui arrive si vous le faites avec une associativité à gauche (si vous voulez que l’arbre reste petit)). Si vous la construisez dans le bon sens, retirez le List.rev.*)}
| i = instruction EOF       { Instruction i }
| e = expression EOF        { Expression e }

_function:
| _type ID L_PAR params R_PAR DEF instruction { Func_decl ($1, $2, $4, $7, Annotation.create $loc)}

function_list:
| _function                 { [$1] }
| _function function_list   { $1::$2 }

param:
| _typeVal ID          { (Value, $1, $2) }
| VAR _typeVal ID      { (Reference, $2, $3) }

params:
| param                     { [$1] }
| param COMMA params        { $1::$3 }
|                           { [] }

instruction:
| _type ID SEMICOLON                                { Var_decl ($1, $2, Annotation.create $loc) }
| _type array SEMICOLON                             { Array_decl ($1, fst $2, snd $2, Annotation.create $loc) }
| ID ASSIGN expression SEMICOLON                    { Affect ($1, $3, Annotation.create $loc) }
| array ASSIGN expression SEMICOLON                 { Affect_array (fst $1, snd $1, $3, Annotation.create $loc) }
| L_CUR_BRK instructions R_CUR_BRK                  { Block ($2, Annotation.create $loc) }
| PRINT expression SEMICOLON                        { Print_expr ($2, Annotation.create $loc) }
| PRINT STRING SEMICOLON                            { Print_str ($2, Annotation.create $loc) }
| IF expression THEN instruction ELSE instruction   { IfThenElse ($2, $4, $6, Annotation.create $loc) }
| WHILE expression instruction                      { While ($2, $3, Annotation.create $loc) }
| RETURN SEMICOLON                                  { Return (None, Annotation.create $loc) }
| RETURN expression SEMICOLON                       { Return (Some $2, Annotation.create $loc) }
| ID L_PAR arguments R_PAR SEMICOLON                { Proc ($1, $3, Annotation.create $loc)}

instructions:
| instruction                       { [$1] }
| instruction instructions          { $1::$2 }

arguments:
| expression                        { [$1] }
| expression COMMA arguments        { $1::$3 }

expression:
| L_PAR expression R_PAR            { $2 }
| INT                               { Cst_i ($1, Annotation.create $loc) }
| FLOAT                             { Cst_f ($1, Annotation.create $loc) }
| BOOL                              { Cst_b ($1, Annotation.create $loc) }
| SUB expression %prec UMIN         { Unop (UMin, $2, Annotation.create $loc) }
| NOT expression                    { Unop (Not, $2, Annotation.create $loc) }
| expression binop expression       { Binop ($2, $1, $3, Annotation.create $loc) }
| ID                                { Var ($1, Annotation.create $loc) }
| array                             { Array_val (fst $1, snd $1, Annotation.create $loc) }
| ID DOT SIZE                       { Size_tab ($1, Annotation.create $loc) }
| ID L_PAR arguments R_PAR          { Func ($1, $3, Annotation.create $loc) }

%inline binop:
| EQ            { Eq }
| NEQ           { Neq }
| LT            { Lt }
| GT            { Gt }
| GEQ           { Geq }
| LEQ           { Leq }
| AND           { And }
| OR            { Or }
| ADD           { Add }
| SUB           { Sub }
| MUL           { Mul }
| DIV           { Div }
| MOD           { Mod }

%inline _type:
| INT_TYP       { TInt }
| FLOAT_TYP     { TFloat }
| BOOL_TYP      { TBool }
| NULL_TYP      { TNull }

%inline _typeVal:
| _type                     { Basic $1 }
| _type L_SQ_BRK R_SQ_BRK   {Array_t $1}

%inline array:
| ID L_SQ_BRK expression R_SQ_BRK { ($1, $3) }
