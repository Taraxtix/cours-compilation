%token ADD SUB
%token MUL DIV REC
%token LPAR RPAR
%token <int> INT
%token EOF

%left ADD
%left SUB
%left MUL
%left DIV

%nonassoc USUB
%nonassoc REC

%start <unit> main


%%

main:
| EOF   {}
| expr EOF {}

expr:
| expr ADD expr {}
| expr SUB expr {}
| expr MUL expr {}
| expr DIV expr {}
| expr REC {}
| LPAR expr RPAR {}
| INT {}
| SUB expr {}
%prec USUB

