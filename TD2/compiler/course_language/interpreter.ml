open Ast
open Abstract_machine

exception Return_exp of value option
exception Non_variable_reference of expr
exception Non_bool_test of expr
exception Non_integer_array_position of expr

let get_tab_pos name pos = name ^ string_of_int pos

(* Commencez à modifier à partir d’ici -- le code présent dans les fonctions n’est là que pour empêcher des warnings à la compilation qui obscurcirait votre sortie. Enlevez-le quand vous commencez à implémenter une fonction.*)

(* Sémantique d’une opération binaire*)
let rec operation_of_binop (op : binop) (v1 : value) (v2 : value) =
  (match (v1, v2) with
  | VNone, _ | _, VNone -> failwith "Binop cannot be applied to VNone operand"
  | VArray _, _ | _, VArray _ ->
      failwith "Binop cannot be applied to VArray operand"
  | _ -> ());

  let vNone_id (_a : value) (_b : value) = VNone in
  let func_of_binop op type_ : value -> value -> value =
    match (op, type_) with
    | Add, TInt -> add_i
    | Add, TFloat -> add_f
    | Sub, TInt -> sub_i
    | Sub, TFloat -> sub_f
    | Mul, TInt -> mul_i
    | Mul, TFloat -> mul_f
    | Div, TInt -> div_i
    | Div, TFloat -> div_f
    | Mod, TInt -> mod_i
    | Mod, TFloat -> mod_f
    | And, TBool -> and_b
    | Or, TBool -> or_b
    | Eq, _ -> eq_m
    | Lt, _ -> lt_m
    | _ -> vNone_id
  and type_basic_of_value value =
    match value with
    | VInt _ -> TInt
    | VFloat _ -> TFloat
    | VBool _ -> TBool
    | _ -> failwith "Unreachable"
  in

  match op with
  | Add | Sub | Mul | Div | Mod | And | Or | Eq | Lt ->
      if type_basic_of_value v1 = type_basic_of_value v2 then
        func_of_binop op (type_basic_of_value v1) v1 v2
      else VNone
  | Neq -> (
      match operation_of_binop Eq v1 v2 with
      | VBool b -> not_b (VBool b)
      | _ -> VNone)
  | Gt -> operation_of_binop Lt v2 v1
  | Leq ->
      operation_of_binop Or
        (operation_of_binop Eq v1 v2)
        (operation_of_binop Lt v1 v2)
  | Geq ->
      operation_of_binop Or
        (operation_of_binop Eq v1 v2)
        (operation_of_binop Lt v2 v1)

(* Sémantique d’une opération unaire*)
let operation_of_unop (op : unop) (v : value) =
  match op with
  | UMin -> (
      match v with
      | VInt _ -> sub_i (VInt 0) v
      | VFloat _ -> sub_f (VFloat 0.) v
      | _ -> VNone)
  | Not -> ( match v with VBool _ -> not_b v | _ -> VNone)

(* Cette fonction interprète une expression et renvoie sa valeur. Vous devez traiter tous les cas possibles (cf module Ast). Reportez-vous au cours pour une explication de la sémantique. On conseille de traiter parallèlement expressions et instructions par ordre de complexité (décrit dans le cours). Vous pouvez laisser des cas non-traités simplement en leur associant [failwith "TODO"] qui fera planter le programme sur ces cas, mais permettra de compiler et de traiter les autres.*)
let rec interpret_expr (map : value Util.Environment.t)
    (map_function : (Ast.argument list * Ast.instruction) Util.Environment.t)
    (expr : Ast.expr) =
  match expr with
  | Cst_i (v, _) -> VInt v
  | Cst_f (v, _) -> VFloat v
  | Cst_b (v, _) -> VBool v
  | Var (v, _) -> (
      match Util.Environment.get map v with
      | Some v -> v
      | None -> failwith ("Variable named '" ^ v ^ "' not found"))
  | Binop (op, arg1, arg2, _) ->
      operation_of_binop op
        (interpret_expr map map_function arg1)
        (interpret_expr map map_function arg2)
  | Unop (op, arg, _) ->
      operation_of_unop op (interpret_expr map map_function arg)
  | Array_val (array_name, expr, _) -> (
      let name, env = get_array_info array_name map in
      match interpret_expr map map_function expr with
      | VInt i -> (
          match Util.Environment.get env (get_tab_pos name i) with
          | Some v -> v
          | None ->
              failwith
                ("Array element ` " ^ get_tab_pos name i ^ "` was not found"))
      | _ -> failwith "Array index must be an integer")
  | Size_tab (name, _) -> (
      let name_prefix, env = get_array_info name map in
      match Util.Environment.get env (name_prefix ^ "size") with
      | Some v -> v
      | None -> failwith ("Array" ^ name ^ " is not declared"))
  | Func (name, param, _) -> (
      match Util.Environment.get map_function name with
      | Some (args_list, body) -> (
          let func_env : value Util.Environment.t =
            Util.Environment.new_environment ()
          in
          let args_params = List.combine args_list param
          and handle ((arg : argument), (param : expr)) _ =
            match arg with
            | Value, _, name ->
                Util.Environment.add func_env name
                  (interpret_expr map map_function param)
            | Reference, _, name -> (
                match param with
                | Var (var_name, _) -> (
                    match Util.Environment.get_ref map var_name with
                    | Some v -> Util.Environment.add_ref func_env name v
                    | None ->
                        failwith ("Variable named '" ^ var_name ^ "' not found")
                    )
                | _ -> failwith "Can only pass by reference a variable")
          in
          List.fold_right handle args_params ();
          interpret_instruction func_env map_function body;
          match Util.Environment.get func_env "#result" with
          | Some v -> v
          | None -> VNone)
      | None -> failwith ("Function named '" ^ name ^ "' not found"))
(*à remplacer par le code : ce code n’est là que pour que le programme compile sans warning.*)

(* Cette fonction interprète une instruction. Le «and» est là pour qu’elle soit co-récursive avec interpret_expr (à cause des appels de fonctions). Elle ne renvoie rien, mais applique directement des effets de bord sur [map]. Reportez-vous au cours pour la sémantique.*)
and interpret_instruction (map : value Util.Environment.t)
    (map_function : (Ast.argument list * Ast.instruction) Util.Environment.t)
    (instruction : Ast.instruction) =
  match instruction with
  | Affect (name, expr, _) ->
      Util.Environment.add map name (interpret_expr map map_function expr)
  | Block (instr_list, _) ->
      List.iter (interpret_instruction map map_function) instr_list
  | IfThenElse (pred, instr1, instr2, _) -> (
      match interpret_expr map map_function pred with
      | VBool b ->
          let instr = if b then instr1 else instr2 in
          interpret_instruction map map_function instr
      | _ -> failwith "Predicate must be a boolean")
  | While (pred, instr, _) -> (
      match interpret_expr map map_function pred with
      | VBool b ->
          if b then (
            interpret_instruction map map_function instr;
            interpret_instruction map map_function instruction)
      | _ -> ())
  | For (pred, increment, body, _) ->(
    match interpret_expr map map_function pred with
      | VBool true ->(
          interpret_instruction map map_function body;
          interpret_instruction map map_function increment;
          interpret_instruction map map_function instruction)
      | _ -> ())
  | Affect_array (array_name, index, v, _) -> (
      let name, env = get_array_info array_name map in
      match interpret_expr map map_function index with
      | VInt i ->
          Util.Environment.add env (get_tab_pos name i)
            (interpret_expr map map_function v)
      | _ -> failwith "Array index must be an integer")
  | Array_decl (_, array_name, size, _) -> (
      match interpret_expr map map_function size with
      | VInt i ->
          Util.Environment.add map array_name (VArray (array_name, map));
          Util.Environment.add map (array_name ^ "size") (VInt i)
      | _ -> failwith "Array size must be an integer")
  | Proc (name, param, _) -> (
      match Util.Environment.get map_function name with
      | Some (args_list, body) ->
          let func_env : value Util.Environment.t =
            Util.Environment.new_environment ()
          in
          let args_params = List.combine args_list param
          and handle ((arg : argument), (param : expr)) _ =
            match arg with
            | Value, _, name ->
                Util.Environment.add func_env name
                  (interpret_expr map map_function param)
            | Reference, _, name -> (
                match param with
                | Var (var_name, _) -> (
                    match Util.Environment.get_ref map var_name with
                    | Some v -> Util.Environment.add_ref func_env name v
                    | None ->
                        failwith ("Variable named '" ^ var_name ^ "' not found")
                    )
                | _ -> failwith "Can only pass by reference a variable")
          in
          List.fold_right handle args_params ();
          interpret_instruction func_env map_function body
      | None -> failwith ("Function named '" ^ name ^ "' not found"))
  | Return (v, _) -> (
      match v with
      | Some e ->
          Util.Environment.add map "#result" (interpret_expr map map_function e)
      | None -> ())
  | Print_str (str, _) -> print_string str
  | Print_expr (expr, _) ->
      print_string (string_of_value (interpret_expr map map_function expr))
  | Var_decl (_, _, _) -> ()

and get_array_info (name : string) (map : value Util.Environment.t) :
    string * value Util.Environment.t =
  match Util.Environment.get map name with
  | Some (VArray (name, env)) -> (name, env)
  | _ -> failwith "Expected an array, got something else"

(*Cette fonction doit interpréter une déclaration de fonction. Elle consiste simplement à associer la liste des arguments et le corps de la fonction à son nom dans l’environnement [functions].*)
let interpret_func_decl
    (functions : (Ast.argument list * Ast.instruction) Util.Environment.t)
    (func_decl : Ast.function_decl) =
  let (Func_decl (_, name, args, body, _)) = func_decl in
  Util.Environment.add functions name (args, body)

(* Cette fonction utilitaire vous est fournie : elle permet de mettre la liste des arguments à la même taille que celle des paramètres de la fonction main : s’il n’y en a pas assez, on leur attribue la valeu VNone, s’il y en a trop, on ignore les derniers. Cela permet de rendre la ligne de commande un peu plus résiliente à un mauvais nombre d’argument sur l’exécution d’un programme*)
let normalize_arg_list args vars =
  if List.length args < List.length vars then
    args @ List.init (List.length vars - List.length args) (fun _ -> "")
  else if List.length args > List.length vars then
    List.filteri (fun i _ -> i < List.length vars) args
  else args

(* Cette fonction permet d’exécuter une liste de déclaration de fonctions sur les arguments passés à la ligne de commande, et lance dessus la fonction main. Elle analyse la liste des fonctions, et stocke leurs définitions dans un environnement de fonctions, puis récupère la définition de la fonction nommée "main", crée un environnement de variables à partir de [args] (normalisées avec la fonction ci-dessus) et de ses paramètres et enfin appelle le corps de main sur ces arguments (comme un appel de fonction, sauf qu’ici les arguments sont directement des objets sémantiques et non syntaxique). Elle est au fond similaire à un appel de fonction, mais un peu plus technique, donc on vous la fourni.*)

let interpret_prg prg args =
  let functions = Util.Environment.new_environment () in
  List.iter (interpret_func_decl functions) prg;
  let environnement = Util.Environment.new_environment () in
  let params, body =
    try Option.get (Util.Environment.get functions "main")
    with _ -> failwith "Function main not defined!"
  in
  let vars = List.map (fun (_, _, v) -> v) params in
  let args = normalize_arg_list args vars in
  List.iter2
    (fun v a ->
      Abstract_machine.parse_complex_argument_and_affect environnement v a)
    vars args;
  try interpret_instruction environnement functions body
  with Return_exp _ -> ()
